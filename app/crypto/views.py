from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, resolve
from django.views.generic import ListView, RedirectView

from crypto.models import Cryptocurrency

from crypto.models import Watchlist


class CryptoListView(ListView):
    model = Cryptocurrency
    context_object_name = "queryset"
    queryset = Cryptocurrency.objects.all().order_by("cmc_rank")
    template_name = "index.html"

    def get_queryset(self):
        user = self.request.user
        context = dict()
        if not user.is_anonymous:
            context["watchlist"] = Watchlist.objects.get(
                user=self.request.user
            ).cryptocurrencies.all()
        context["cryptocurrency_list"] = Cryptocurrency.objects.all().order_by(
            "cmc_rank"
        )
        return context


class AddToWatchlistView(RedirectView):
    url = reverse_lazy("crypto_list")

    def get(self, request, *args, **kwargs):
        user = self.request.user
        watchlist = Watchlist.objects.get(user=user)
        cryptocurrency = Cryptocurrency.objects.get(id=self.kwargs["uuid"])
        watchlist.cryptocurrencies.add(cryptocurrency)

        return super(AddToWatchlistView, self).get(request, *args, **kwargs)


class RemoveFromWatchlistView(RedirectView):
    def get(self, request, *args, **kwargs):
        user = self.request.user
        watchlist = Watchlist.objects.get(user=user)
        cryptocurrency = Cryptocurrency.objects.get(id=self.kwargs["uuid"])
        watchlist.cryptocurrencies.remove(cryptocurrency)

        return super(RemoveFromWatchlistView, self).get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return self.request.META.get("HTTP_REFERER")


class WatchlistView(LoginRequiredMixin, ListView):
    model = Watchlist
    context_object_name = "cryptocurrency_list"
    template_name = "watchlist.html"
    login_url = "/users/login/"

    def get_queryset(self):
        watchlist = Watchlist.objects.get(user=self.request.user)
        return watchlist.cryptocurrencies.all().order_by("cmc_rank")
