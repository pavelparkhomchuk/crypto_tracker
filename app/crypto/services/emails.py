from crypto.models import Watchlist
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


def send_email_with_watchlist():
    watchlists = Watchlist.objects.all()
    mail_subject = "Cryptocurrencies rates"
    for item in watchlists:
        # Skip email sending if watchlist is empty
        if item.cryptocurrencies.exists():
            message = render_to_string(
                "emails/watchlist_email.html",
                {"watchlist": item.cryptocurrencies.all()},
            )
            email = EmailMessage(
                subject=mail_subject, body=message, to=(item.user.email,)
            )
            email.content_subtype = "html"
            email.send(fail_silently=False)
