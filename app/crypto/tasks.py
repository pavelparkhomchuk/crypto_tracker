import json

import requests

from celery import shared_task

from crypto.models import Cryptocurrency
from django.contrib.auth.models import User

from crypto.services.emails import send_email_with_watchlist


@shared_task(autoretry_for=(Exception,), retry_backoff=True)
def get_crypto_currencies():
    url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest"
    headers = {
        "Accepts": "application/json",
        "X-CMC_PRO_API_KEY": "d70334d0-1fbc-4730-85b1-5560ff9002e1",
    }
    params = {
        "id": "1,1027,825,3408,1839,4687,52,2010,5426,74,6636,4943,1958,5994,5805,3717,3957,3890,7083,2,"
        + "4195,3635,1975,512,6535,4030,3794,1321,328,1831,4558,3077,1966,18876,2011,4642,6210,8916,"
        + "2416,6783,2280,5665,6892,2563,3602,2087,1518,1765,7278,3330,1437,16086,2502,1720,10791,"
        + "5068,3897,6719,3155,3513,19891,4256,4157,1376,4066,1274,4847,4705,1697,18069,2469,1934,"
        + "131,2130,7186,5034,4846,8104,7080,8642,5567,6538,2682,8646,6945,873,2694,5692,1168,4269,"
        + "5632,2634,1659,1684,5647,8677,1772,3801,2083,7455,1808",
        "convert": "USD",
    }
    resp = requests.get(url, headers=headers, params=params)
    data = json.loads(resp.text)["data"]

    for key, value in data.items():
        Cryptocurrency.objects.update_or_create(
            name=value["name"],
            symbol=value["symbol"],
            defaults={
                "price": round(value["quote"]["USD"]["price"], 5),
                "cmc_rank": value["cmc_rank"],
            },
        )


@shared_task(autoretry_for=(Exception,), retry_backoff=True)
def send_email_with_watchlist_task():
    send_email_with_watchlist()
