from django.urls import path

from crypto.views import (
    CryptoListView,
    AddToWatchlistView,
    WatchlistView,
    RemoveFromWatchlistView,
)

urlpatterns = [
    path("", CryptoListView.as_view(), name="crypto_list"),
    path(
        "add-to-watchlist/<uuid:uuid>/",
        AddToWatchlistView.as_view(),
        name="add_to_watchlist",
    ),
    path(
        "remove-from-watchlist/<uuid:uuid>/",
        RemoveFromWatchlistView.as_view(),
        name="remove_from_watchlist",
    ),
    path("watchlist/", WatchlistView.as_view(), name="watchlist"),
]
