from django.db.models.signals import post_save
from django.conf import settings
from django.dispatch import receiver

from crypto.models import Watchlist


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_watchlist(sender, instance, created, **kwargs):
    if created:
        Watchlist.objects.create(user=instance)
