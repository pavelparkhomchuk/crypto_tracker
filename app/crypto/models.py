import uuid

from django.db import models
from django.conf import settings


class Cryptocurrency(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, null=False)
    symbol = models.CharField(max_length=50, null=False)
    price = models.FloatField(null=False)
    cmc_rank = models.IntegerField(null=True)


class Watchlist(models.Model):
    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True
    )
    cryptocurrencies = models.ManyToManyField(to=Cryptocurrency)

    def __str__(self):
        return f"{self.user} watchlist"
