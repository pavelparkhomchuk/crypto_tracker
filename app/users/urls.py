from django.urls import path
from users.views import SignUpView, ActivateUserView

urlpatterns = [
    path("sign-up/", SignUpView.as_view(), name="sign_up"),
    path(
        "activate/<str:uuid64>/<str:token>", ActivateUserView.as_view(), name="activate"
    ),
]
