from django.conf import settings
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth.models import User
from django.forms import EmailField, EmailInput


class CustomUserCreationForm(UserCreationForm):
    email = EmailField(required=True, widget=EmailInput(attrs={"class": "validate"}))

    class Meta:
        model = User
        fields = ("username", "email")
        field_classes = {
            "username": UsernameField,
        }
