from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, RedirectView

from users.forms import CustomUserCreationForm

from users.services.emails import send_registration_email

from users.token import TokenGenerator


class SignUpView(SuccessMessageMixin, CreateView):
    template_name = "registration/sign-up.html"
    success_url = reverse_lazy("login")
    form_class = CustomUserCreationForm
    success_message = "Your profile was created successfully"

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        send_registration_email(self.request, user)
        return super().form_valid(form)


class ActivateUserView(RedirectView):
    url = reverse_lazy("crypto_list")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            user_pk = force_bytes(urlsafe_base64_decode(uuid64))
            current_user = User.objects.get(pk=user_pk)
        except (User.DoesNotExist, ValueError, TypeError):
            HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user)
            return super().get(request, *args, **kwargs)
        HttpResponse("Wrong data")
