#!/bin/sh

celery -A app.celeryapp:app worker --loglevel=info --concurrency=4 &
celery -A app.celeryapp:app beat &
python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate
python manage.py check
gunicorn app.wsgi:application --bind 0.0.0.0:8000
