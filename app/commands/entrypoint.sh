#!/bin/sh

celery -A app.celeryapp:app worker --loglevel=debug --concurrency=4 &
celery -A app.celeryapp:app beat &
python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate
python manage.py check
python manage.py runserver 0:8000
